import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

import CalcStart        from '@/views/calc/1-start.vue';
import CalcCustomer     from '@/views/calc/2-customer.vue';
import CalcImpl         from '@/views/calc/3-implementation.vue';
import CalcRequirements from '@/views/calc/4-requirements.vue';
import CalcReview       from '@/views/calc/5-review.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/calc/start',
      name: 'calc-start',
      component: CalcStart,
    },
    {
      path: '/calc/customer',
      name: 'calc-customer',
      component: CalcCustomer,
    },
    {
      path: '/calc/implementation',
      name: 'calc-implementation',
      component: CalcImpl,
    },
    {
      path: '/calc/requirements',
      name: 'calc-requirements',
      component: CalcRequirements,
    },
    {
      path: '/calc/review',
      name: 'calc-review',
      component: CalcReview,
    },
  ],
});
