import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


export default new Vuex.Store({
  state: {
    calcNavItems: [
        {href: '/calc/start', name: 'Start'},
        {href: '/calc/customer', name: 'Customer Details'},
        {href: '/calc/implementation', name: 'Implementation Details'},
        {href: '/calc/requirements', name: 'Required Capabilities'},
        {href: '/calc/review', name: 'Review'},
    ],
    training: [
      {id: 'git',     name: 'GitLab with Git Basics', trainingUnits: 1},
      {id: 'cicd',    name: 'GitLab CI/CD Training', trainingUnits: 1},
      {id: 'admin',   name: 'GitLab Admin Training', trainingUnits: 1},
      {id: 'pm',      name: 'Project Management with GitLab', trainingUnits: 1},
      {id: 'special', name: 'Specialized Training', trainingUnits: 1},
    ],
  },
  mutations: {

  },
  actions: {

  },
});
