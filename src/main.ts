import Vue from 'vue';
import 'materialize-css';

import App from './App.vue';
import router from './router';
import store from './store';

import { initMaterlize } from '@/lib/init.js';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
  mounted() {
    initMaterlize();
  },
}).$mount('#app');
